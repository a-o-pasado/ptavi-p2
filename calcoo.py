import sys


class Calculadora:
    def __init__(self, dato1, dato2):
        self.numero1 = dato1
        self.numero2 = dato2

    def suma(self):
        return self.numero1 + self.numero2

    def resta(self):
        return self.numero1 - self.numero2


if __name__ == "__main__":
    try:
        dato1 = int(sys.argv[1])
        operacion = sys.argv[2]
        dato2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error:Non numerical parametres")
    objeto = Calculadora(dato1, dato2)

    if operacion == "+":
        result = objeto.suma()
    elif operacion == "-":
        result = objeto.resta()
    else:
        sys.exit("Pero si solo podes sumar o restar BOLUDO.")
    print(result)
