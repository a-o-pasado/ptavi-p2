import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def producto(self):
        return self.numero1 * self.numero2

    def division(self):
        try:
            return self.numero1 / self.numero2
        except ZeroDivisionError:
            print("No se puede dividir por cero")


if __name__ == "__main__":
    try:
        dato1 = int(sys.argv[1])
        operacion = sys.argv[2]
        dato2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error:Non numerical parametres")
    objeto = CalculadoraHija(dato1, dato2)

    if operacion == "+":
        result = objeto.suma()
    elif operacion == "-":
        result = objeto.resta()
    elif operacion == "x":
        result = objeto.producto()
    elif operacion == "/":
        result = objeto.division()
    else:
        sys.exit("Pero si solo podes sumar, restar, multiplicar o dividir BOLUDO.")
    print(result)
