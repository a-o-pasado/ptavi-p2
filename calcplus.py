import sys
import calcoohija

if __name__ == "__main__":
    archivo = open(sys.argv[1])
    lista = archivo.readlines()

    for line in lista:

        elementos = line.split(",")
        operando = elementos[0]
        temporal = int(elementos[1])

        for numero in elementos[2:]:

            operador = int(numero)
            calculo = calcoohija.CalculadoraHija(temporal, operador)

            if operando == "suma":
                temporal = calculo.suma()

            elif operando == "resta":
                temporal = calculo.resta()

            elif operando == "multiplica":
                temporal = calculo.producto()

            elif operando == "divide":
                temporal = calculo.division()

            else:
                sys.exit("Pero si solo podes sumar, restar, multiplicar o dividir BOLUDO.")
        print(temporal)
