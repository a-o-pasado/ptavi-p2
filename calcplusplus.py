import sys
import calcoohija
import csv

with open(sys.argv[1], newline="") as archivo:
    lista = csv.reader(archivo)

    for line in lista:

        operando = line[0]
        temporal = int(line[1])

        for numero in line[2:]:

            operador = int(numero)
            calculo = calcoohija.CalculadoraHija(temporal, operador)

            if operando == "suma":
                temporal = calculo.suma()

            elif operando == "resta":
                temporal = calculo.resta()

            elif operando == "multiplica":
                temporal = calculo.producto()

            elif operando == "divide":
                temporal = calculo.division()

            else:
                sys.exit("Pero si solo podes sumar, restar, multiplicar o dividir BOLUDO.")
        print(temporal)
